unit alumno;
interface
  const
    ARCHIALUS = 'alumnos.dat';
  type
    TFecha = array[1..3] of integer; { año, mes, día }
    
    TAlumno = record
      matricula: integer;
      nombre: string[80];
      ingreso: TFecha; { año, mes, día }
      { TODO: otra opción sería precalcular este dato de la siguiente forma: }
      { esto se calcula al ejecutar el programa o al hacer carga de datos }
      { también se re calcula al aprobar un final }
      { se almacena en el archivo, de esta forma se agiliza dicha consulta }
      { regular: boolean; }
    end;
    TAlumnos = ^TPunAlu;
    TPunAlu = record
      info: TAlumno;
      der, izq: TAlumnos;
    end;
    TAAlumnos = file of TAlumno;

  procedure fechaVacia(var fecha: TFecha);
  function fechaEsVacia(var fecha: TFecha): boolean;
  procedure fechaIngresar(var fecha: TFecha);
  function fechaIgualAnio(const x,y: TFecha): boolean;
  procedure fechaCopiar(var x: TFecha; const y: TFecha);
  procedure alumnoAgregar(var alumnos: TAlumnos);
  procedure alumnoListar(raiz: TAlumnos);
  procedure alumnoCargar(var raiz: TAlumnos);
  procedure alumnoGuardar(raiz: TAlumnos);
  function alumnoBuscar(a: TAlumnos; matricula: integer): TAlumnos;

implementation
  procedure fechaVacia(var fecha: TFecha);
  begin
    fecha[1] := 0;
    fecha[2] := 0;
    fecha[3] := 0;
  end;
  
  function fechaEsVacia(var fecha: TFecha): boolean;
  begin
    fechaEsVacia := (fecha[1] = 0) OR (fecha[2] = 0) OR (fecha[3] = 0);
  end;
  
  procedure fechaIngresar(var fecha: TFecha);
  begin
    writeln('  Día del mes:');
    readln(fecha[3]);
    writeln('  Mes:');
    readln(fecha[2]);
    writeln('  Año:');
    readln(fecha[1]);
  end;
  
  function fechaIgualAnio(const x,y: TFecha): boolean;
  begin
    fechaIgualAnio := x[1] = y[1];
  end;
  
  procedure fechaCopiar(var x: TFecha; const y: TFecha);
  begin
    x[1] := y[1];
    x[2] := y[2];
    x[3] := y[3];
  end;

  procedure alumnoMeter(var raiz: TAlumnos; el: TAlumno);
  { es solo un macro para no repetir la tarea de inserción mínima }
  begin
    New(raiz);
    raiz^.info.matricula  := el.matricula;
    raiz^.info.nombre     := el.nombre;
    raiz^.info.ingreso[1] := el.ingreso[1];
    raiz^.info.ingreso[2] := el.ingreso[2];
    raiz^.info.ingreso[3] := el.ingreso[3];
    raiz^.der := NIL;
    raiz^.izq := NIL;
  end;

  procedure alumnoInsertar(var raiz: TAlumnos; el: TAlumno);
  var
    a: TAlumnos;
    ins: Boolean;
  begin
    if raiz = NIL then
      alumnoMeter(raiz, el)
    else
    begin
      a := raiz;
      ins := FALSE;
      while not ins do
      begin
        if el.matricula < a^.info.matricula then
        begin
          if a^.izq = NIL then
          begin
            alumnoMeter(a^.izq, el);
            ins := TRUE;
          end
          else
            a := a^.izq;
        end
        else
        begin
          if a^.der = NIL then
          begin
            alumnoMeter(a^.der, el);
            ins := TRUE;
          end
          else
            a := a^.der;
        end;
      end;
    end;
  end;

  procedure alumnoDestroyR(var raiz: TAlumnos);
  { TODO: analizar de pasar a no recursivo }
  begin
    if raiz <> NIL Then
    begin
      alumnoDestroyR(raiz^.izq);
      alumnoDestroyR(raiz^.der);
      Dispose(raiz);
    end;
  end;

  procedure alumnoAgregar(var alumnos: TAlumnos);
  var
    alumno: TAlumno;
    faux: TFecha;
    cade: String;
  begin
    writeln('Ingrese el número de matrícula del alumno:');
    readln(alumno.matricula);
    writeln('Ingrese apellidos y nombres del alumno:');
    readln(alumno.nombre);
    writeln('Fecha de ingreso del alumno:');
    fechaIngresar(alumno.ingreso);
    {
    writeln('Fecha de nacimiento:');
    fechaIngresar(faux);
    writeln('Lugar de nacimiento:');
    readln(cade);
    writeln('Domicilio:');
    readln(cade);
    writeln('Localidad:');
    readln(cade);
    writeln('DNI:');
    readln(cade);
    writeln('Título secundario:');
    readln(cade);
    }
    alumnoInsertar(alumnos, alumno);
  end;

  procedure alumnoCargar(var raiz: TAlumnos);
  var
    archivo: TAAlumnos;
    alumno: TAlumno;
  begin
    alumnoDestroyR(raiz);
    raiz := NIL;
    assign(archivo, ARCHIALUS);
    reset(archivo);
    while not eof(archivo) do
    begin
      read(archivo, alumno);
      alumnoInsertar(raiz, alumno);
    end;
    close(archivo);
  end;

  procedure alumnoGuardarR(raiz: TAlumnos; var archivo:TAAlumnos);
  { TODO: eliminar la recursión }
  { se escribe usando preOrder para que en la próxima lectura, el árbol quede balanceado }
  begin
    if raiz <> NIL then
    begin
      write(archivo, raiz^.info);
      alumnoGuardarR(raiz^.izq, archivo);
      alumnoGuardarR(raiz^.der, archivo);
    end;
  end;

  procedure alumnoGuardar(raiz: TAlumnos);
  { TODO: eliminar la recursión }
  var
    archivo: TAAlumnos;
  begin
    assign(archivo, ARCHIALUS);
    erase(archivo);
    rewrite(archivo);
    alumnoGuardarR(raiz, archivo);
    close(archivo);
  end;

  procedure alumnoListar(raiz: TAlumnos);
  { TODO: analizar de eliminar la recursión }
  { se lista usando inOrder según su código }
  begin
    if raiz <> NIL then
    begin
      alumnoListar(raiz^.izq);
      writeln(raiz^.info.matricula, '-', raiz^.info.nombre, ', ingreso: ', raiz^.info.ingreso[3], '/', raiz^.info.ingreso[2], '/', raiz^.info.ingreso[1]);
      alumnoListar(raiz^.der);
    end;
  end;

  function alumnoBuscar(a: TAlumnos; matricula: integer): TAlumnos;
  var
    ret: TAlumnos;
  begin
    ret := NIL;
    while (a <> NIL) AND (ret = NIL) do
    begin
      if a^.info.matricula = matricula then
        ret := a
      else
        if a^.info.matricula < matricula then
          a := a^.der
        else
          a := a^.izq;
    end;
    alumnoBuscar := ret;
  end;
begin
end.
