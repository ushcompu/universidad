unit inscripcion;
interface
  uses alumno, materia;
  const
    ARCHIINSCR = 'inscripciones.dat';

  type
    TInscripcion = record
      alumno: TAlumnos;
      materia: TMaterias;
      estado: integer;
      cursada: TFecha; { fecha de cursada }
      finales: array[1..3] of TFecha; { las 3 fechas posibles de finales/libres }
    end;
    TAInscripcion = record
      alumno, materia, estado: integer;
      cursada: TFecha; { fecha de cursada }
      finales: array[1..3] of TFecha; { las 3 fechas posibles de finales/libres }
    end;
    TInscripciones = ^TPunInscr;
    TPunInscr = record
      info: TInscripcion;
      der, izq: TInscripciones;
    end;
    TAInscripciones = file of TAInscripcion;

  procedure inscrCargar(var raiz: TInscripciones; materias: TMaterias; alumnos: TAlumnos);
  procedure inscrGuardar(raiz: TInscripciones);
  procedure inscrRegularI(inscripciones: TInscripciones; materias: TMaterias; alumnos: TAlumnos);
  procedure inscrAgregar(var inscripciones: TInscripciones; alumnos: TAlumnos; materias: TMaterias);
  procedure inscrAlumnos(inscripciones: TInscripciones; materias: TMaterias; alumnos: TAlumnos);
  procedure inscrMateriaXAlumno(inscripciones: TInscripciones; alumnos: TAlumnos);

implementation
  procedure inscrMeter(var raiz: TInscripciones; el: TInscripcion; insertar: boolean);
  { es solo un macro para no repetir la tarea de inserción mínima }
  begin
    if insertar then { inserta o actualiza }
      new(raiz);
    raiz^.info.alumno        := el.alumno;
    raiz^.info.materia       := el.materia;
    raiz^.info.estado        := el.estado;
    raiz^.info.cursada[1]    := el.cursada[1];
    raiz^.info.cursada[2]    := el.cursada[2];
    raiz^.info.cursada[3]    := el.cursada[3];
    fechaCopiar(raiz^.info.finales[1], el.finales[1]);
    fechaCopiar(raiz^.info.finales[2], el.finales[2]);
    fechaCopiar(raiz^.info.finales[3], el.finales[3]);
    if insertar then
    begin
      raiz^.der := NIL;
      raiz^.izq := NIL;
    end;
  end;

  function inscrMenor(x,y: TInscripcion): boolean;
  begin
    inscrMenor := (x.alumno^.info.matricula < y.alumno^.info.matricula)
                   OR ((x.alumno^.info.matricula = y.alumno^.info.matricula)
                  AND (x.materia^.info.codigo < y.materia^.info.codigo));
  end;

  procedure inscrInsertar(var raiz: TInscripciones; el: TInscripcion);
  var
    a: TInscripciones;
    ins: Boolean;
  begin
    if raiz = NIL then
      inscrMeter(raiz, el, TRUE)
    else
    begin
      a := raiz;
      ins := FALSE;
      while not ins do
      begin
        if (el.alumno =  a^.info.alumno) AND (el.materia =  a^.info.materia) then
        begin
          inscrMeter(a, el, FALSE);
          ins := TRUE;
        end
        else
          if inscrMenor(el, a^.info) then
          begin
            if a^.izq = NIL then
            begin
              inscrMeter(a^.izq, el, TRUE);
              ins := TRUE;
            end
            else
              a := a^.izq;
          end
          else
          begin
            if a^.der = NIL then
            begin
              inscrMeter(a^.der, el, TRUE);
              ins := TRUE;
            end
            else
              a := a^.der;
          end;
      end;
    end;
  end;

  procedure inscrDestroyR(var raiz: TInscripciones);
  { TODO: analizar de pasar a no recursivo }
  begin
    if raiz <> NIL Then
    begin
      inscrDestroyR(raiz^.izq);
      inscrDestroyR(raiz^.der);
      Dispose(raiz);
    end;
  end;

  function inscrBuscar(a: TInscripciones; el: TInscripcion): TInscripciones;
  var
    ret: TInscripciones;
  begin
    ret := NIL;
    if (el.alumno <> NIL) AND (el.materia <> NIL) then
      while (a <> NIL) AND (ret = NIL) do
      begin
        if (a^.info.materia = el.materia) AND (a^.info.alumno = el.alumno) then
          ret := a
        else
          if inscrMenor(el, a^.info) then
            a := a^.izq
          else
            a := a^.der;
      end;
    inscrBuscar := ret;
  end;

  function inscrEnEstado(inscripciones: TInscripciones; alumno: TAlumnos; materia: TMaterias; estado: integer): boolean;
  { para ver si una materia está en cierto estado, o sea, si está cursada o aprobada }
  var
    inscr: TInscripcion;
  begin
    inscr.materia := materia;
    inscr.alumno  := alumno;
    inscripciones := inscrBuscar(inscripciones, inscr);
    inscrEnEstado := (inscripciones <> NIL) AND (inscripciones^.info.estado = estado);
  end;

  procedure inscrAgregar(var inscripciones: TInscripciones; alumnos: TAlumnos; materias: TMaterias);
  var
    matricula, codigo, i: integer;
    opcion: char;
    inscripcion: TInscripcion;
    puede: boolean;
    auxMat: TMaterias;
    auxInscr: TInscripciones;
  begin
    writeln ('Ingrese la matrícula del alumno a inscribir:');
    readln(matricula);
    inscripcion.alumno := alumnoBuscar(alumnos, matricula);
    if inscripcion.alumno = NIL then
      writeln('Error: matrícula de alumno inexistente.')
    else
    begin
      writeln ('Ingrese el código de la materia:');
      readln(codigo);
      inscripcion.materia := materiaBuscar(materias, codigo);
      if inscripcion.materia = NIL then
        writeln('Error: código de materia inexistente.')
      else
      begin
        repeat
          writeln ('    1. Sólo para cursar');  { estado 0 }
          writeln ('    2. Cursada aprobada');  { estado 1 }
          writeln ('    3. Para rendir final'); { estado 2 }
          writeln ('    4. Final aprobado');    { estado 3 }
          readln(opcion);
        until (ord(opcion) >= ord('1')) AND (ord(opcion) <= ord('4'));
        inscripcion.estado := ord(opcion) - ord('1');

        puede := TRUE;
        case inscripcion.estado of
          0: { para cursar }
          begin
            i := 1;
            while (puede) AND (i < MAXCORRE) AND (inscripcion.materia^.info.corres[i] <> 0) do
            begin
              auxMat := materiaBuscar(materias, inscripcion.materia^.info.corres[i]);
              { vamos a buscar estado 1 que es cursada aprobada }
              puede := (auxMat = NIL) OR inscrEnEstado(inscripciones, inscripcion.alumno, auxMat, 1);
            end;
            if puede then
            begin
              fechaVacia(inscripcion.finales[1]);
              fechaVacia(inscripcion.finales[2]);
              fechaVacia(inscripcion.finales[3]);
              writeln('Ingrese la fecha de inscripción a la cursada:');
              fechaIngresar(inscripcion.cursada);
            end
            else
              writeln('No se puede inscribir, no tiene alguna de las correlativas cursadas.');
          end;
          1: { cursada aprobada }
          begin
            fechaVacia(inscripcion.finales[1]);
            fechaVacia(inscripcion.finales[2]);
            fechaVacia(inscripcion.finales[3]);
            auxInscr := inscrBuscar(inscripciones, inscripcion);
            if auxInscr = NIL then
              fechaCopiar(inscripcion.cursada, auxInscr^.info.cursada)
            else
              writeln('El alumno no se inscribió a la materia.');
          end;
          2:
          begin
            auxInscr := inscrBuscar(inscripciones, inscripcion);
            { al menos el último final deberá estar disponible }
            puede := (auxInscr = NIL) OR (fechaEsVacia(auxInscr^.info.finales[3]));
            if puede then
            begin
              i := 1;
              if auxInscr <> NIL then
              begin
                while (puede) AND (i < MAXCORRE) AND (auxInscr^.info.materia^.info.corres[i] <> 0) do
                begin
                  auxMat := materiaBuscar(materias, auxInscr^.info.materia^.info.corres[i]);
                  { vamos a buscar estado 3 que es final aprobado }
                  puede := (auxMat = NIL) OR inscrEnEstado(inscripciones, inscripcion.alumno, auxMat, 3);
                  inc(i);
                end;
                if not(puede) then
                  writeln('No se puede inscribir, debe aprobar los finales de las correlativas.');
                for i := 1 to 3 do
                  fechaCopiar(inscripcion.finales[i], auxInscr^.info.finales[i]);
                i := 1;
                while (i <= 3) AND (not(fechaEsVacia(inscripcion.finales[i]))) do
                  inc(i);
              end;
              writeln('Ingrese fecha de inscripción al final:');
              fechaIngresar(inscripcion.finales[i]);
            end
            else
              writeln('No tiene mas posibilidades de finales, ya rindió 3 veces la materia.');
          end;
          3:
          begin
            auxInscr := inscrBuscar(inscripciones, inscripcion);
            puede := (auxInscr <> NIL) AND (auxInscr^.info.estado = 2);
            if puede then
            begin
              i := 1;
              while (puede) AND (i <= MAXCORRE) AND (inscripcion.materia^.info.corres[i] <> 0) do
              begin
                auxMat := materiaBuscar(materias, inscripcion.materia^.info.corres[i]);
                { vamos a buscar estado 3 que es final aprobado }
                puede := (auxMat = NIL) OR inscrEnEstado(inscripciones, inscripcion.alumno, auxMat, 3);
                inc(i);
              end;
              if not(puede) then
                writeln('No se puede inscribir, debe aprobar los finales de las correlativas.');
            end
            else
              writeln('No puede rendir el final, no se inscribió al mismo.');
          end;
        end;
        if puede then
          inscrInsertar(inscripciones, inscripcion);
      end;
    end;
  end;

  procedure inscrCargar(var raiz: TInscripciones; materias: TMaterias; alumnos: TAlumnos);
  var
    archivo: TAInscripciones;
    info: TAInscripcion;
    inscr: TInscripcion;
  begin
    inscrDestroyR(raiz);
    raiz := NIL;
    assign(archivo, ARCHIINSCR);
    reset(archivo);
    while not eof(archivo) do
    begin
      read(archivo, info);
      fechaCopiar(inscr.cursada, info.cursada);
      fechaCopiar(inscr.finales[1], info.finales[1]);
      fechaCopiar(inscr.finales[2], info.finales[2]);
      fechaCopiar(inscr.finales[3], info.finales[3]);
      inscr.estado := info.estado;
      inscr.materia := materiaBuscar(materias, info.materia);
      inscr.alumno := alumnoBuscar(alumnos, info.alumno);
      if inscr.materia = NIL then
        writeln('Error no existe la materia con código: ', info.materia)
      else
        if inscr.alumno = NIL then
          writeln('Error no existe el alumno con matrícula: ', info.alumno)
        else
          inscrInsertar(raiz, inscr);
    end;
    close(archivo);
  end;

  procedure inscrGuardarR(raiz: TInscripciones; var archivo:TAInscripciones);
  { TODO: eliminar la recursión }
  { se escribe usando preOrder para que en la próxima lectura, el árbol quede balanceado }
  var
    info: TAInscripcion;
  begin
    if raiz <> NIL then
    begin
      fechaCopiar(info.cursada, raiz^.info.cursada);
      fechaCopiar(info.finales[1], raiz^.info.finales[1]);
      fechaCopiar(info.finales[2], raiz^.info.finales[2]);
      fechaCopiar(info.finales[3], raiz^.info.finales[3]);
      info.estado := raiz^.info.estado;
      info.materia := raiz^.info.materia^.info.codigo;
      info.alumno := raiz^.info.alumno^.info.matricula;
      write(archivo, info);
      inscrGuardarR(raiz^.izq, archivo);
      inscrGuardarR(raiz^.der, archivo);
    end;
  end;

  procedure inscrGuardar(raiz: TInscripciones);
  { TODO: eliminar la recursión }
  var
    archivo: TAInscripciones;
  begin
    assign(archivo, ARCHIINSCR);
    erase(archivo);
    rewrite(archivo);
    inscrGuardarR(raiz, archivo);
    close(archivo);
  end;

  function inscrContarFinalesR(inscr: TInscripciones; alumnos: TAlumnos; ret: integer): integer;
  begin
    if inscr <> NIL then
    begin
      if inscr^.info.alumno = alumnos then
      begin
        if inscr^.info.estado = 3 then { final aprobado }
          inc(ret);
        ret := inscrContarFinalesR(inscr^.izq, alumnos, ret);
        ret := inscrContarFinalesR(inscr^.der, alumnos, ret);
      end
      else
        if alumnos^.info.matricula < inscr^.info.alumno^.info.matricula then
          ret := inscrContarFinalesR(inscr^.izq, alumnos, ret);
        if alumnos^.info.matricula > inscr^.info.alumno^.info.matricula then
          ret := inscrContarFinalesR(inscr^.der, alumnos, ret);
    end;
    inscrContarFinalesR := ret;
  end;

  function inscrRegular(inscripciones: TInscripciones; materias: TMaterias; alumnos: TAlumnos; matricula: integer; const fecha: TFecha): boolean;
  var
    { precondición: parámetro fecha >= ingreso del alumno }
    finales, anios: integer; { cantidad de años en la carrera desde que se inscribió }
    ret: boolean;
  begin
    ret := FALSE;
    alumnos := alumnoBuscar(alumnos, matricula);
    if alumnos <> NIL then
    begin
      anios := fecha[1] - alumnos^.info.ingreso[1] + 1;
      finales := inscrContarFinalesR(inscripciones, alumnos, 0);
      if anios = 1 then
        ret := finales > 0
      else
        ret := (finales div anios) > 1;
    end
    else
      writeln('Error: matrícula de alumno inexistente.');
    inscrRegular := ret;
  end;

  procedure inscrAlumnosR(inscripciones: TInscripciones; materias: TMaterias; alumnos: TAlumnos; const fecha: TFecha);
  { TODO: analizar de eliminar la recursión }
  { se lista usando inOrder según su código }
  begin
    if alumnos <> NIL then
    begin
      inscrAlumnosR(inscripciones, materias, alumnos^.izq, fecha);
      write(alumnos^.info.matricula, ' - ', alumnos^.info.nombre, ', ingreso: ', alumnos^.info.ingreso[3]);
      write('/', alumnos^.info.ingreso[2], '/', alumnos^.info.ingreso[1]);
      if inscrRegular(inscripciones, materias, alumnos, alumnos^.info.matricula, fecha) then
        writeln(' ES regular')
      else
        writeln(' NO ES regular');
      inscrAlumnosR(inscripciones, materias, alumnos^.der, fecha);
    end;
  end;

  procedure inscrAlumnos(inscripciones: TInscripciones; materias: TMaterias; alumnos: TAlumnos);
  var
    fecha: TFecha;
  begin
    writeln('Ingrese la fecha para consultar regularidad (ejemplo: fecha de hoy):');
    fechaIngresar(fecha);
    inscrAlumnosR(inscripciones, materias, alumnos, fecha);
  end;

  procedure inscrRegularI(inscripciones: TInscripciones; materias: TMaterias; alumnos: TAlumnos);
  var
    fecha: TFecha;
    matricula: integer;
  begin
    writeln('Ingrese matrícula del alumno:');
    readln(matricula);
    writeln('Ingrese la fecha para consultar regularidad (ejemplo: fecha de hoy):');
    fechaIngresar(fecha);
    if inscrRegular(inscripciones, materias, alumnos, matricula, fecha) then
      writeln('El alumno es regular')
    else
      writeln('El alumno NO es regular');
  end;

  procedure inscrMateriaXAlumnoR(inscripciones: TInscripciones; alumnos: TAlumnos);
  begin
    if inscripciones <> NIL then
    begin
      if inscripciones^.info.alumno <> alumnos then
        inscrMateriaXAlumnoR(inscripciones^.izq, alumnos);
      if inscripciones^.info.alumno = alumnos then
      begin
        inscrMateriaXAlumnoR(inscripciones^.izq, alumnos);
        materiaWrite(inscripciones^.info.materia^.info);
        writeln;
        write('  Estado: ');
        case inscripciones^.info.estado of
          0: writeln('cursando.');
          1: writeln('cursada aprobada.');
          2: writeln('inscripto a final.');
          3: writeln('final aprobado.');
        end;
        inscrMateriaXAlumnoR(inscripciones^.der, alumnos);
      end
      else
        inscrMateriaXAlumnoR(inscripciones^.der, alumnos);
    end;
  end;

  procedure inscrMateriaXAlumno(inscripciones: TInscripciones; alumnos: TAlumnos);
  var
    matricula: integer;
  begin
    writeln('Ingrese matrícula del alumno:');
    readln(matricula);
    alumnos := alumnoBuscar(alumnos, matricula);
    if (alumnos <> NIL) then
    begin
      inscrMateriaXAlumnoR(inscripciones, alumnos);
    end
    else
      writeln('Error: matrícula de alumno inexistente.');
  end;

begin
end.
