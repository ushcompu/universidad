program tp02;
{ Algorítmica y Programación 2 }
{ Alumno: Matias Russitto }
{ Licencia: MIT }
{ Mes: 201107 }

uses inscripcion, alumno, materia;

procedure cargarDatos(var materias: TMaterias; var alumnos: TAlumnos; var inscripciones: TInscripciones);
begin
  writeln('Cargando datos...');
  materiaCargar(materias);
  alumnoCargar(alumnos);
  inscrCargar(inscripciones, materias, alumnos);
end;

procedure guardarDatos(materias: TMaterias; alumnos: TAlumnos; inscripciones: TInscripciones);
begin
  writeln('Guardando datos...');
  materiaGuardar(materias);
  alumnoGuardar(alumnos);
  inscrGuardar(inscripciones);
end;

procedure menu(var materias: TMaterias; var alumnos: TAlumnos; var inscripciones: TInscripciones);
var
  opcion: char;
{ TODO: mejorar el menú, agregando menú principal y submenú por módulo (materias, alumnos, inscripciones) }
{ otro opción es usar números, y todo lo que empiece con 1 sea de un módulo, todo lo de 2 con otro, y así... }
begin
  repeat
    writeln('---------------------------------------------------');
    writeln ('Ingrese el número de la opción que desea realizar:');
    writeln ('  0. Cargar datos (se borrarán los datos actuales)');
    writeln ('  1. Inscribir alumno en la carrera');
    writeln ('  2. Agregar materia');
    writeln ('  3. ¿Es regular?');
    { al inscribrir se le podrá decir en qué estado, si es para cursar, para rendir final, para poner como cursada o como aprobada }
    writeln ('  4. Inscribir en materia');
    writeln ('  5. Listar alumnos');
    writeln ('  6. Listar materias');
    writeln ('  7. Listar materias de alumno');
    writeln ('  8. Listar correlativas');
    writeln ('  9. Salir sin grabar');
    writeln ('  a. Grabar y salir');
    writeln('---------------------------------------------------');
    readln (opcion);

    case opcion of
      '0': cargarDatos(materias, alumnos, inscripciones);
      '1': alumnoAgregar(alumnos);
      '2': materiaAgregar(materias);
      '3': inscrRegularI(inscripciones, materias, alumnos);
      '4': inscrAgregar(inscripciones, alumnos, materias);
      '5': inscrAlumnos(inscripciones, materias, alumnos);
      '6': materiaListar(materias);
      '7': inscrMateriaXAlumno(inscripciones, alumnos);
      '8': materiaCorrelativas(materias);
      'a': guardarDatos(materias, alumnos, inscripciones);
    end;
  until (opcion = '9') OR (opcion = 'a');
end;

var
  materias: TMaterias;
  alumnos: TAlumnos;
  inscripciones: TInscripciones;
begin
  writeln('Bienvenido');
  { hacer por defecto la carga de datos }
  cargarDatos(materias, alumnos, inscripciones);
  menu(materias, alumnos, inscripciones);
  writeln('Adiós');
end.
