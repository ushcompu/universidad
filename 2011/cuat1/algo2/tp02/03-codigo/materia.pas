unit materia;
interface
  const
    { cantidad de correlatitvas x materia, según requerimiento (enunciado) }
    MAXCORRE = 5;
    ARCHIMATES = 'materias.dat';
  type
  TMateria = record
    { se decide no usar alfanuméricos en los códigos, así las búsquedas son mas rápidas }
    codigo: integer;
    nombre: string[50];
    carga: integer;
    { FALSE: primer cuatri, TRUE: segundo cuatri. Este tipo se usa para optimizar el tamaño de datos, y su facilidad de uso }
    cuatri: Boolean;
    corres : array[1..MAXCORRE] of integer; { se almacenan los códigos }
  end;
  TMaterias = ^TPunMat;
  TPunMat = record
    info: TMateria;
    der, izq: TMaterias;
  end;
  TAMaterias = file of TMateria;

  procedure materiaAgregar(var materias: TMaterias);
  procedure materiaListar(raiz: TMaterias);
  { cargar y guardar desde y hasta el archivo TODAS las materias }
  procedure materiaCargar(var raiz: TMaterias);
  procedure materiaGuardar(raiz: TMaterias);
  procedure materiaCorrelativas(raiz: TMaterias);
  function materiaBuscar(a: TMaterias; codigo: integer): TMaterias;
  procedure materiaWrite(const materia: TMateria);

implementation
  procedure materiaMeter(var raiz: TMaterias; el: TMateria);
  { es solo un macro para no repetir la tarea de inserción mínima }
  begin
    New(raiz);
    raiz^.info.codigo    := el.codigo;
    raiz^.info.nombre    := el.nombre;
    raiz^.info.carga     := el.carga;
    raiz^.info.cuatri    := el.cuatri;
    raiz^.info.corres[1] := el.corres[1];
    raiz^.info.corres[2] := el.corres[2];
    raiz^.info.corres[3] := el.corres[3];
    raiz^.info.corres[4] := el.corres[4];
    raiz^.info.corres[5] := el.corres[5];
    raiz^.der := NIL;
    raiz^.izq := NIL;
  end;

  procedure materiaInsertar(var raiz: TMaterias; el: TMateria);
  var
    a: TMaterias;
    ins: Boolean;
  begin
    if raiz = NIL then
      materiaMeter(raiz, el)
    else
    begin
      a := raiz;
      ins := FALSE;
      while not ins do
      begin
        if el.codigo < a^.info.codigo then
        begin
          if a^.izq = NIL then
          begin
            materiaMeter(a^.izq, el);
            ins := TRUE;
          end
          else
            a := a^.izq;
        end
        else
        begin
          if a^.der = NIL then
          begin
            materiaMeter(a^.der, el);
            ins := TRUE;
          end
          else
            a := a^.der;
        end;
      end;
    end;
  end;

  procedure materiaDestroyR(var raiz: TMaterias);
  { TODO: analizar de pasar a no recursivo }
  begin
    if raiz <> NIL Then
    begin
      materiaDestroyR(raiz^.izq);
      materiaDestroyR(raiz^.der);
      Dispose(raiz);
    end;
  end;

  procedure materiaAgregar(var materias: TMaterias);
  var
    materia: TMateria;
    i, cuatri: integer;
  begin
    writeln('Ingrese el número de código de la materia:');
    readln(materia.codigo);
    writeln('Ingrese nombre de la materia:');
    readln(materia.nombre);
    writeln('Ingrese la carga horaria de la materia:');
    readln(materia.carga);
    writeln('Ingrese el cuatrimestre de la materia (1 ó 2):');
    readln(cuatri);
    materia.cuatri := cuatri = 2;
    for i := 1 to MAXCORRE do
    begin
      if (i = 1) OR (materia.corres[i-1] <> 0) then
      begin
        writeln('Ingrese la materia ', i, ' correlativa de la materia o 0 en el caso que no tenga:');
        readln(materia.corres[i]);
      end
      else
        materia.corres[i] := 0;
    end;
    materiaInsertar(materias, materia);
  end;

  procedure materiaCargar(var raiz: TMaterias);
  var
    archivo: TAMaterias;
    materia: TMateria;
  begin
    materiaDestroyR(raiz);
    raiz := NIL;
    assign(archivo, ARCHIMATES);
    reset(archivo);
    while not eof(archivo) do
    begin
      read(archivo, materia);
      materiaInsertar(raiz, materia);
    end;
    close(archivo);
  end;

  procedure materiaGuardarR(raiz: TMaterias; var archivo:TAMaterias);
  { TODO: eliminar la recursión }
  { se escribe usando preOrder para que en la próxima lectura, el árbol quede balanceado }
  begin
    if raiz <> NIL then
    begin
      write(archivo, raiz^.info);
      materiaGuardarR(raiz^.izq, archivo);
      materiaGuardarR(raiz^.der, archivo);
    end;
  end;

  procedure materiaGuardar(raiz: TMaterias);
  { TODO: eliminar la recursión }
  var
    archivo: TAMaterias;
  begin
    assign(archivo, ARCHIMATES);
    erase(archivo);
    rewrite(archivo);
    materiaGuardarR(raiz, archivo);
    close(archivo);
  end;

  procedure materiaWrite(const materia: TMateria);
  var
    cuatri, i: integer;
  begin
    if materia.cuatri then cuatri := 2 else cuatri := 1;
    writeln(materia.codigo, '-', materia.nombre, ', carga: ', materia.carga, ', cuatrimestre: ', cuatri);
    write('  Correlativas:');
    for i := 1 to MAXCORRE do
      if materia.corres[i] > 0 then
        write(' ', materia.corres[i]);
  end;

  procedure materiaListar(raiz: TMaterias);
  { TODO: analizar de eliminar la recursión }
  { se lista usando inOrder según su código }
  begin
    if raiz <> NIL then
    begin
      materiaListar(raiz^.izq);
      materiaWrite(raiz^.info);
      writeln;
      materiaListar(raiz^.der);
    end;
  end;

  function materiaBuscar(a: TMaterias; codigo: integer): TMaterias;
  var
    ret: TMaterias;
  begin
    ret := NIL;
    while (a <> NIL) AND (ret = NIL) do
    begin
      if a^.info.codigo = codigo then
        ret := a
      else
        if a^.info.codigo < codigo then
          a := a^.der
        else
          a := a^.izq;
    end;
    materiaBuscar := ret;
  end;

  procedure materiaImprimir(raiz: TMaterias; codigo: integer);
  { es simplemente una auxiliar de buscarMateria, para modular la impresión de datos de una materia }
  { precondición: a <> NIL }
  var
    cuatri, i: integer;
    a, b: TMaterias;
  begin
    a := materiaBuscar(raiz, codigo);
    if a <> NIL then
    begin
      if a^.info.cuatri then cuatri := 2 else cuatri := 1;
      writeln(a^.info.codigo, '-', a^.info.nombre, ', carga: ', a^.info.carga, ', cuatrimestre: ', cuatri);
      if a^.info.corres[1] > 0 then
      begin
        writeln('  Correlativas:');
        i := 1;
        while (i <= MAXCORRE) AND (a^.info.corres[i] > 0) do
        begin
          b := materiaBuscar(raiz, a^.info.corres[i]);
          if b <> NIL then
            writeln('    ', b^.info.codigo, '-', b^.info.nombre, ', carga: ', b^.info.carga, ', cuatrimestre: ', cuatri);
          inc(i);
        end;
      end;
    end
    else
      writeln('No existe una materia con ese código');
  end;

  procedure materiaCorrelativas(raiz: TMaterias);
  var
    codigo: integer;
  begin
    writeln('Ingrese código de materia a buscar:');
    readln(codigo);
    materiaImprimir(raiz, codigo);
  end;
begin
end.
