unit tpunopibe;
interface
  const
    MAXPIBES = 5;
  type
    { Los pibes y las figus son un nombre }
    TNombre = string[30];
    TPibes = array[1..MAXPIBES] of TNombre;
    { Tipo Archivo de Pibes }
    TAPibes = file of TNombre;

  function  nombrePibe(var pibes: TPibes; iPibe: integer):TNombre;
  function  numeroPibe(var pibes: TPibes; nombre: TNombre):integer;
  procedure cargarPibes(var pibes: TPibes);
  procedure  crearPibes();
  procedure    verPibes(var pibes:TPibes);

implementation
  const
    ARCHIPIBES = 'pibes.dat';

  function nombrePibe(var pibes: TPibes; iPibe: integer):TNombre;
  begin
    nombrePibe := pibes[iPibe];
  end;

  function numeroPibe(var pibes: TPibes; nombre: TNombre):integer;
    {
      los pibes estAn desordenados ya que son solo 5.
      si fueran mas se verIa de optimizar con un previo ordenamiento.
    }
  var
    i, ret: integer;
  begin
    ret := 0;
    i := 1;
    while (i <= MAXPIBES) and (ret = 0) do
    begin
      if pibes[i] = nombre then
        ret := i;
      i := i + 1;
    end;
    numeroPibe := ret;
  end;

  procedure cargarPibeTeclado(var pibes: TPibes; i: integer);
  begin
    write('Ingrese el nombre del pibe: ');
    readln(pibes[i]);
  end;

  procedure cargarPibes(var pibes: TPibes);
  var
    archivo: TAPibes;
    i: integer;
  begin
    i := 1;
    assign(archivo,ARCHIPIBES);
    reset(archivo);
    while (not eof(archivo)) AND (i <= MAXPIBES) do
    begin
      read(archivo, pibes[i]);
      i := i + 1;
    end;

    if not eof(archivo) then
      writeln('Hay mas pibes que ',MAXPIBES,', solo se cargaron ',MAXPIBES,' pibes.');

    while i <= MAXPIBES do
    begin
      cargarPibeTeclado(pibes, i);
      i := i + 1;
    end;
  end;

  procedure crearPibes();
  { crea con MAXPIBES (5) nombres de pibes reescribiendo o creando el archivo }
  var
    archivo: TAPibes;
  begin
    assign(archivo,ARCHIPIBES);
    rewrite(archivo);
    write(archivo,'Juan');
    write(archivo,'Nico');
    write(archivo,'Fede');
    write(archivo,'Emi');
    write(archivo,'Mati');
    close(archivo);
  end;

  procedure verPibes(var pibes:TPibes);
  var
    i: integer;
  begin
    for i := 1 to MAXPIBES do
      writeln(i:2,') ',pibes[i]);
  end;

begin
end.
