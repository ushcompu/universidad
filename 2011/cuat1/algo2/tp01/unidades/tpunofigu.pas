unit tpunofigu;
interface
  const
    MAXFIGUS = 20;
  type
    { Los pibes y las figus son un nombre }
    TNombre = string[30];
    TFigus = array[1..MAXFIGUS] of TNombre;
    { Tipo Archivo de Figus }
    TAFigus = file of TNombre;

  function  nombreFigu(var figus: TFigus; iFigu: integer):TNombre;
  function  numeroFigu(var figus: TFigus; nombre: TNombre):integer;
  procedure cargarFigus(var figus: TFigus);
  procedure  crearFigus();
  procedure    verFigus(var figus:TFigus);

implementation
  const
    ARCHIFIGUS = 'figus.dat';

  function nombreFigu(var figus: TFigus; iFigu: integer):TNombre;
  begin
    nombreFigu := figus[iFigu];
  end;

  function numeroFigu(var figus: TFigus; nombre: TNombre):integer;
    {
      los figus estAn desordenados ya que son solo 5.
      si fueran mas se verIa de optimizar con un previo ordenamiento.
    }
  var
    i, ret: integer;
  begin
    ret := 0;
    i := 1;
    while (i <= MAXFIGUS) and (ret = 0) do
    begin
      if figus[i] = nombre then
        ret := i;
      i := i + 1;
    end;
    numeroFigu := ret;
  end;

  procedure cargarFiguTeclado(var figus: TFigus; i: integer);
  begin
    write('Ingrese el nombre del figu: ');
    readln(figus[i]);
  end;

  procedure cargarFigus(var figus: TFigus);
  var
    archivo: TAFigus;
    i: integer;
  begin
    i := 1;
    assign(archivo,ARCHIFIGUS);
    reset(archivo);
    while (not eof(archivo)) AND (i <= MAXFIGUS) do
    begin
      read(archivo, figus[i]);
      i := i + 1;
    end;

    if not eof(archivo) then
      writeln('Hay mas figus que ',MAXFIGUS,', solo se cargaron ',MAXFIGUS,' figus.');

    while i <= MAXFIGUS do
    begin
      cargarFiguTeclado(figus, i);
      i := i + 1;
    end;
  end;

  procedure crearFigus();
  { crea con MAXFIGUS (20) nombres de figus reescribiendo o creando el archivo }
  var
    archivo: TAFigus;
  begin
    assign(archivo,ARCHIFIGUS);
    rewrite(archivo);
    { van sin tildes ni enies ya que eso ocupa dos bytes y no alinea en la impresiOn }
    write(archivo,'Ubaldo Fillol');
    write(archivo,'Luis Galvan');
    write(archivo,'Daniel Killer');
    write(archivo,'Jorge Olguin');
    write(archivo,'Miguel Oviedo');
    write(archivo,'Ruben Pagnanini');
    write(archivo,'Daniel Passarella');
    write(archivo,'Alberto Tarantini');
    write(archivo,'Norberto Alonso');
    write(archivo,'Osvaldo Ardiles');
    write(archivo,'Americo Gallego');
    write(archivo,'Ruben Galvan');
    write(archivo,'Omar Larrosa');
    write(archivo,'Daniel Valencia');
    write(archivo,'Julio Villa');
    write(archivo,'Daniel Bertoni');
    write(archivo,'Rene Houseman');
    write(archivo,'Mario Kempes');
    write(archivo,'Leopoldo Luque');
    write(archivo,'Oscar Ortiz');
    close(archivo);
  end;

  procedure verFigus(var figus:TFigus);
  var
    i: integer;
  begin
    for i := 1 to MAXFIGUS do
      writeln(i:2,') ',figus[i]);
  end;

begin
end.
