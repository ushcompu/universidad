unit tpunoalbus;
interface
  const
    MAXPIBES = 5;
    MAXFIGUS = 20;
  type
    TAlbumes = array[1..MAXPIBES,1..MAXFIGUS] of integer;
    TAAlbumes = file of integer;
    TNombre = string[30];
    TPibes = array[1..MAXPIBES] of TNombre;
    TFigus = array[1..MAXFIGUS] of TNombre;
    TArray = array [1..MAXPIBES] of integer;

  procedure crearAlbus();
  procedure cargarAlbus(var matriz: TAlbumes);
  procedure verAlbus(var matriz: TAlbumes; var pibes:TPibes; var figus:TFigus);
  procedure intercambiarUna(var matriz: TAlbumes; pibe1,pibe2: integer; var intercambiO: boolean);
  function intercambio(var matriz: TAlbumes; pibe1,pibe2: integer): integer;

  procedure figusfaltan (var matriz:TAlbumes; numpibe:integer; var pibes:TPibes; var figus:TFigus);
  procedure albumcompleto (var matriz:TAlbumes; var pibes:TPibes; var figus:TFigus);
  procedure variospibes (var matriz:TAlbumes; var orden:Tarray; var pibes:TPibes);
  function chequeo (orden:Tarray):boolean;

implementation
  const
    ARCHIALBUS = 'albumes.dat';
  { el pibe1 le da al pibe2 la figu1, y el pibe2 le da al pibe1 la figu2 }
  procedure intercambioReal(var matriz: TAlbumes; pibe1,pibe2,figu1,figu2: integer);
  begin
    matriz[pibe1][figu1] := matriz[pibe1][figu1] - 1;
    matriz[pibe2][figu1] := matriz[pibe2][figu1] + 1;
    matriz[pibe1][figu2] := matriz[pibe1][figu2] + 1;
    matriz[pibe2][figu2] := matriz[pibe2][figu2] - 1;
  end;

  procedure intercambiarUna(var matriz: TAlbumes; pibe1,pibe2: integer; var intercambiO: boolean);
  var
    j, tenta1, figu: integer;
  begin
    j := 1;
    tenta1 := 0;
    intercambiO := FALSE;
    while (not intercambiO) and (j <= MAXFIGUS) do
    begin
      if tenta1 = 0 then
      begin
        { aun buscamos la primer figurita tentativa, e indicamos el tentativo primer receptor }
        if (matriz[pibe1][j] = 0) and (matriz[pibe2][j] > 1) then
        begin
          figu := j;
          tenta1 := pibe1;
        end;
        if (matriz[pibe2][j] = 0) and (matriz[pibe1][j] > 1) then
        begin
          figu := j;
          tenta1 := pibe2;
        end;
      end
      else
      begin
        { a buscar por cual intercambiar }
        if (tenta1 = pibe1) and (matriz[pibe2][j] = 0) and (matriz[pibe1][j] > 1) then
        begin
          { la cosa real, el intercambio en si mismo }
          intercambioReal(matriz,pibe1,pibe2,j,figu);
          intercambiO := TRUE;
        end;
        if (tenta1 = pibe2) and (matriz[pibe1][j] = 0) and (matriz[pibe2][j] > 1) then
        begin
          { la cosa real, el intercambio en si mismo }
          intercambioReal(matriz,pibe2,pibe1,j,figu);
          intercambiO := TRUE;
        end;
      end;
      j := j + 1;
    end;
  end;

  function intercambio(var matriz: TAlbumes; pibe1,pibe2: integer): integer;
  var
    cant: integer;
    haIntercambiado: boolean;
  begin
    cant := 0;
    intercambiarUna(matriz,pibe1,pibe2,haIntercambiado);
    while haIntercambiado do
    begin
      cant:=cant+1;
      intercambiarUna(matriz,pibe1,pibe2,haIntercambiado);
    end;
    intercambio := cant;
  end;

  procedure crearAlbus();
  { crea con MAXFIGUS (20) nombres de figus reescribiendo o creando el archivo }
  var
    archivo: TAAlbumes;
    i: integer;
  begin
    assign(archivo,ARCHIALBUS);
    rewrite(archivo);
    for i := 1 to (MAXPIBES * MAXFIGUS) do
      write(archivo,random(5));
    close(archivo);
  end;

  procedure cargarAlbus(var matriz: TAlbumes);
  var
    archivo: TAAlbumes;
    i,j: integer;
  begin
    i:=1;
    j:=1;

    assign(archivo,ARCHIALBUS);
    reset(archivo);
    while (not eof(archivo)) AND (j <= MAXFIGUS) do
    begin
      read(archivo,matriz[i][j]);
      j := j + 1;
      if j > MAXFIGUS then
      begin
        j := 1;
        i := i + 1;
      end;
    end;
    close(archivo);
  end;

  procedure verAlbus(var matriz: TAlbumes; var pibes:TPibes; var figus:TFigus);
  var
    i,j: integer;
  begin
    write('                  ');
    for i := 1 to MAXPIBES do
      write(pibes[i]:5,' ');
    writeln;

    for j := 1 to MAXFIGUS do
    begin
      write(figus[j]:18);
      for i := 1 to MAXPIBES do
      begin
        write(matriz[i][j]:5,' ');
      end;
      writeln;
    end;
  end;

  { chequea que no haya repetidos numero de pibes repetidos }
  function chequeo (orden:Tarray):boolean;
  var
   i,j:integer;
   repetido:boolean;
  begin
      i:=1;
      repetido:= false;
      while (i<MAXPIBES) and (repetido=false) do
      begin
         j:=i+1;
         while (j<=MAXPIBES) and (repetido=false) do
          begin
            if (orden[i]=orden[j]) then
                repetido:=true;
            j:=j+1;
          end;
         i:=i+1;
      end;
      chequeo:=repetido;
  end;

  { solo imprime las figus que le faltan }
  procedure figusfaltan (var matriz:TAlbumes; numpibe:integer; var pibes:TPibes; var figus:TFigus);
  var
   i:integer;
   falta:boolean;
   nombre: TNombre;
  begin
       nombre:= pibes[numpibe];
       falta := FALSE;
       writeln ('A ',nombre,' le falta las figuritas:');
       for i:= 1 to MAXFIGUS do
       begin
          if matriz[numpibe][i]=0 then
          begin
              falta := TRUE;
              writeln (i:3,' ',figus[i]);
          end;
       end;
       if not falta then
           writeln('Ninguna, tiene el album lleno!');
  end;{figusfaltan}

  { intercambia segun entre varios, segun el orden de los pibes }
  procedure variospibes (var matriz:TAlbumes; var orden:Tarray; var pibes:TPibes);
  var
    i,j,cant:integer;
  begin
      for i:=1 to (MAXPIBES-1) do
      begin
          for j:=(i+1) to MAXPIBES do
          begin
             writeln('Intercambiando entre ',pibes[orden[i]],' y ',pibes[orden[j]]);
             {el intercambio es de a un pibe a la vez}
             cant := intercambio(matriz,orden[i],orden[j]);
             writeln('Intercambiaron ',cant,' figuritas.');
          end;
      end;
  end;{variospibes}

 { solo imprime que pibes tienen el album completo }
  procedure albumcompleto (var matriz:TAlbumes; var pibes:TPibes; var figus:TFigus);
  var
    i,j:integer;
    nadie,falta:boolean;
    nombre:TNombre;
  begin
     nadie := true;
     for i:= 1 to MAXPIBES do {hay que chequear los 5 pibes}
     begin
          j:=1;
          falta:=false;
          while (j<=MAXFIGUS) and (not falta) do {donde encuentra que le falta una ya corta}
          begin
              if matriz[i][j]=0 then
                 falta:=true;
              j:=j+1;{esta fuera del if}
          end;
           if (falta=false) then {verifica que salio, y tiene el album completo}
             begin
                nombre:= pibes[i];
                nadie := false;
                writeln(nombre:10,' tiene el album completo.');
             end;
     end;
     if nadie then
        writeln('Nadie tiene el album completo.');
  end; {albumcompleto}
begin
end.
