program tp01;
{ Algoritmica y Programacion 2 }
{ Alumnos: Emiliano Van den Heuvel y Matias Russitto }
uses tpunopibe,tpunofigu,tpunoalbus;

const
  MAXMENU = 7;

function menu:integer;
var
opcion:integer;
begin
  writeln;
  writeln ('Ingrese el numero de la opcion que desea realizar:');
  writeln (' 1.Carga de datos');
  writeln (' 2.Impresion de la situacion');
  writeln (' 3.Figuritas que faltan');
  writeln (' 4.Intercambio entre dos pibes');
  writeln (' 5.Intercambio total');
  writeln (' 6.Coleccion completa');
  writeln (' 7.Fin');
  writeln;
  readln (opcion);
  writeln;
  menu:=opcion;
end;

var
  pibes: TPibes;
  figus: TFigus;
  i,opcion,pibe,pibe1,pibe2:integer;
  nombre: TNombre;
  matriz:TAlbumes;
  orden:TArray;

begin
  opcion:=0;
  {lo obligo a entrar, porque podria preguntar antes pero si ingresa una opcion incorrecta
  cuantas veces verifico con un if?, por eso me parecio mejor obligarlo a entrar}
  while (opcion<>MAXMENU) do
  begin
    opcion:=menu;
  if (opcion<1) or (opcion>MAXMENU) then {chequea que la opcion ingresada sea correcta}
      Writeln('opcion incorrecta')
  else
    case opcion of
      1: begin
            cargarPibes(pibes);
            cargarFigus(figus);
            cargarAlbus(matriz);
         end;
      2: begin
            verPibes(pibes);
            writeln;
            verFigus(figus);
            writeln;
            verAlbus(matriz,pibes,figus);
            writeln;
         end;
      3:  begin
            write('Ingrese el pibe que desea saber: ');  {definir que leer si el numero o el nombre}
            readln(nombre);
            pibe := numeroPibe(pibes,nombre);
            if (pibe<1) or (pibe>MAXPIBES) then     {chequea que el valor sea correcto}
               writeln('Nombre de pibe invalido.')
            else
               figusfaltan(matriz,pibe,pibes,figus); {llama al procedimiento figusfaltan}
            end;
      4:begin
           write('Ingrese el primer pibe que desea intercambiar:  ');
           readln(nombre);
           pibe1 := numeroPibe(pibes,nombre);
           if (pibe1<1) or (pibe1>MAXPIBES) then
              writeln('Nombre del primer pibe invalido.')
           else
           begin
              write('Ingrese el segundo pibe que desea intercambiar: ');
              readln(nombre);
              pibe2 := numeroPibe(pibes,nombre);
              if (pibe2<1) or (pibe2>MAXPIBES) then
                  writeln('Nombre del primer pibe invalido.')
              else
              begin
                  i:=intercambio(matriz,pibe1,pibe2);
                  writeln('Se intercambiaron ',i,' figuritas.');
              end;
           end;
         end;
       5: begin
           i:=1;
           while (i<=MAXPIBES) do
           begin
               write('Ingrese el pibe ',i,' para hacer el intercambio: ');
               readln (pibe);
               if (pibe<1) or (pibe>MAXPIBES) then {chequea que el valor sea correcto}
                  writeln('Numero de pibe invalido.')
               else
                  begin
                    orden[i]:=pibe;
                    i:=i+1;
                   end;
             end;{while}
          if not chequeo(orden) then
              variospibes (matriz,orden,pibes)
          else
             writeln ('Ha ingresados pibe repetidos.');
          end;
       6: albumcompleto(matriz,pibes,figus);

       end;{case}
    end;{while}
end.{program}
