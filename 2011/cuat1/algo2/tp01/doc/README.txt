.. -*- coding: utf-8 -*-

====================================
 Trabajo integrador 01 1° cuat 2011
====================================

.. |date| date:: %Y-%m-%d %H:%M
.. |version| date:: %Y%m%d

:Author: Emiliano Van den Heuvel & Matias Russitto
:Contact: emilianovdh@hotmail.com & matias@russitto.com
:Version: |version|.01
:Date: |date|
:Copyright: Este documento es de dominio público

.. contents:: Contenidos

--------------------------------
 Algorítimica y programación II
--------------------------------

Primer cuatrimestre año 2011. Universidad TDF.

Profesores
==========

* Marcela Jeréz
* Beatriz Depetris

----------------
 Copyright 2011
----------------

* Emiliano Van den Heuvel
* Matias Russitto

--------------------
 License / Licencia
--------------------

Source Code / Código fuente
===========================

New BSD License

Documentation / Documentación
=============================

Public domain

-------
 Notas
-------

* Los .dat son archivos de datos para el programa, se pueden generar otros datos cambiando la sección correspondiente en el programa.

-------------
 Compilación
-------------

* Cómo compilar en GNU/Linux con Free Pascal

  make nix

* Cómo compilar en GNU/Linux con Free Pascal generando .exe para win32

  make win32

* Como limpiar binarios en GNU/Linux:

  make clean

* Para hacer equivalentes a los anteriores pero en Windows observar el archivo de texto Makefile.
