program numerico;
  function convertir(cadena:string):longint;
  var
    i,pot,n,cero,nueve:integer;
    ret:longint;
  begin
    i:=pos('%',cadena);
    if i=0 then i:=length(cadena) else i:=i-1;

    pot:=1;
    ret:=0;

    cero:=ord('0');
    nueve:=ord('9');

    while(i>0)do
    begin
      n:=ord(cadena[i]);
      if(n>=cero)and(n<=nueve)then
      begin
        ret:=ret+(n-cero)*pot;
        pot:=pot*10;
      end;
      i:=i-1;
    end;
    convertir:=ret;
  end;

var
  cadena:string;
begin
  readln(cadena);
  writeln(convertir(cadena));
end.
