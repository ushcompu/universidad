program ncursestest; 

uses    ncurses; 

 
var     x,y: longint; 
 
begin 
        initscr (); 
        echo (); 
 
        x := getmaxx (stdscr); 
        y := getmaxy (stdscr); 
 
        endwin  (); 
 
        writeln ('X: ', x); 
        writeln ('Y: ', y); 
end. 
