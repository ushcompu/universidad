program profesora;
{
  Lo dio la profesora Marcela el 23/03/2011
Se leen un connjunto de palabras terminadas en blanco dentro de un string, ralizar un algoritmo que determine e informe la longitud de cada palabra.
}
procedure contarPalabras(cadena:string);
var
  i,tam,posi:integer;
begin
  tam:=length(cadena);
  i:=1;
  while(i<=tam)do
  begin
    if(cadena[i]=' ')then
    begin
      cadena[i]:='0';
      i:=i+1;
    end
    else
    begin
      posi:=pos(' ',cadena);
      if(posi=0)then
      begin
        writeln(tam-i+1);
        i:=tam+1;
      end
      else
      begin
        writeln(posi-i);
        i:=posi;
      end;
    end;
  end;
end;

var
  cadena:string;
begin
  write('Ingrese cadena: ');
  readln(cadena);
  contarPalabras(cadena);
end.
